﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Osi.Domain;
using OSI.Application.Exceptions;
using OSI.Application.Responses;
using OSI.Infrastructure.Persistence.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Application.Logic.AddNewAddresInHouse.Command.Create
{
    public class CreateNewAddresInHouseCommand : IRequest<Response<int>>
    {
        public int Number { get; set; }
        public int DomId { get; set; }
    }


    public class CreateNewAddresInHouseCommandHandler : IRequestHandler<CreateNewAddresInHouseCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IOSIContext _context;

        public CreateNewAddresInHouseCommandHandler(IMapper mapper, IOSIContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Response<int>> Handle(CreateNewAddresInHouseCommand request, CancellationToken cancellationToken)
        {
            var isExist = await _context.Flat.AnyAsync(u => u.Number == request.Number);
            var flat = _mapper.Map<Flat>(request);

            if (!isExist)
            {
                _context.Flat.Add(flat);
                var result = await _context.SaveChangesAsync(CancellationToken.None);
          
                return new Response<int>(result);
            }
            else
                throw new ApiException($"{flat} not create");
        }
    }
}
