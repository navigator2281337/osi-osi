﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Osi.Domain;
using OSI.Application.Responses;
using OSI.Infrastructure.Persistence.Context;
using OSI.Infrastructure.Persistence.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Application.Logic
{
    public class GenerateData : IGen
    {
        private readonly IOSIContext _context;
        private readonly IMapper _mapper;

        public GenerateData()
        {
        }

        public GenerateData(IOSIContext context)
        {
            _context = context;
        }

        public GenerateData(IOSIContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void AddCities(int numberОfCities = 10)
        {
            for (int city = 1; city <= numberОfCities; city++)
                _context.Cities.Add(new City { Name = "Город" + city });
        }

        public void AddStreets(int number = 250, bool isRandom = false)
        {
            //_context.Cities.AsQueryable().Count();

            foreach (City city in _context.Cities.AsQueryable())
            {
                //int number_of_streets = !isRandom ? number : rnd.Next(number);

                for (int street = 1; street <= number; street++)
                    _context.Streets.Add(new Street { Name = "Улица" + street, City = city });

            }
        }

        public void AddHouses(int number = 20, bool isRandom = false)
        {
            //Log($"Adding of SOME houses for ({_context.Streets.AsQueryable().Count()}) streets has started.\r\n");

            foreach (Street street in _context.Streets.AsQueryable())
            {
                //int number_of_houses = !isRandom ? number : rnd.Next(number);

                for (int house = 1; house <= number; house++)
                    _context.Doms.Add(new Dom { Number = house, Street = street });
            }

        }


        public void AddFlats(int number = 40, bool isRandom = false)
        {
            //Log($"Adding of SOME houses for ({_context.Streets.AsQueryable().Count()}) streets has started.\r\n");
           
            foreach (Dom dom in _context.Doms.AsQueryable())
            {
                //int number_of_houses = !isRandom ? number : rnd.Next(number);

                for (int flat = 1; flat <= number; flat++)
                    _context.Flat.Add(new Flat { Number = flat, Dom = dom });
            }

        }

        public void AddOsi(int number = 500, bool isRandom = false)
        {
                for (int osi = 1; osi <= number; osi++)
                    _context.Osi.Add(new Osi.Domain.Osi {Name = "УК" + osi });
        }


        public void AddRanges()
        {
            using (OSIContext db = new OSIContext())
            {
                GenerateData generateData = new GenerateData(db);

                int c = 1; 



                foreach (Dom dom in _context.Doms.AsQueryable())
                {
                    for (int osi = 1; osi <= 500; osi++)
                    {
                        c++;
                        if (c == 500)
                        {
                            db.SaveChanges();
                        }
                        _context.RangeFlatOsi.Add(new Osi.Domain.RangeFlatOsi { OsiId = osi, Dom = dom, From = 1, To = 40 });
                    }
                }
            }
           
        }

        public void SeedDBUsingSeeder()
        {
            using (OSIContext db = new OSIContext())
            {
                GenerateData generateData = new GenerateData(db);

                generateData.AddCities();
                db.SaveChanges();

                generateData.AddStreets();
                db.SaveChanges();

                generateData.AddHouses();
                db.SaveChanges();

                generateData.AddFlats();
                db.SaveChanges();

                generateData.AddOsi();
                db.SaveChanges();

                generateData.AddRanges();
                //db.SaveChanges();
            }       
        }




























        //public  void Gen()
        //{

        //    int num = 1;
        //    const int maxSaveString = 1000;
        //    DateTime timeStart = DateTime.Now;
        //    for (int city = 1; city <= 10; city++)
        //    {

        //        var CityGen = new City() {Name = "Город" + city };
        //        _context.Cities.Add(CityGen);


        //        for (int street = 1; street <= 250; street++)
        //        {
        //            var streetGen = new Street { Name = "Улица" + street, CityId = city };
        //            _context.Streets.Add(streetGen);

        //            for (int house = 1; house <= 20; house++)
        //            {
        //                var houseGen = new Dom { Number = house, StreetId = street };
        //               _context.Doms.Add(houseGen);
        //                ((OSIContext)_context).SaveChanges();
        //            //for (int flat = 1; flat <= 40; flat++)
        //            //{
        //            //    for (int i = 1; i <= 20; i++)
        //            //    {
        //            //        _context.Flat
        //            //      .Add(new Flat {/* Id = flat,*/ Number = flat, HouseId = i });

        //            //num++;
        //            //if (num == maxSaveString)
        //            //{
        //            //    ((OSIContext)_context).SaveChanges();
        //            //    num = 1;
        //            //}
        //            //    }


        //            }
        //        }
        //        //}

        //    }


        //    //for (int osi = 1; osi <= 500; osi++)
        //    //{
        //    //    _context.Osi.Add(new Osi.Domain.Osi { Id = osi, Name = "УК" + osi });
        //    //}
        //    //        ((OSIContext)_context).SaveChanges();


        //    DateTime timeEnd = DateTime.Now;

        //    Console.WriteLine(timeEnd - timeStart);









        //    // var a =  _context.Osi.Add(new Osi.Domain.Osi {Id = 11,Name = "321312" });

        //    //var group = _mapper.Map<Osi.Domain.Osi>(a);

        //    //((OSIContext)_context).SaveChanges();

        //}
    }


}







    //    private readonly IOSIContext _context;

//    public GenerateData()
//    {
//    }

//    public GenerateData(IOSIContext context)
//    {
//        _context = context;
//    }

//    public void Gen()
//    {


//        //int num = 1;
//        //const int  maxSaveString = 1000;
//        //DateTime timeStart = DateTime.Now;
//        //for (int city = 1; city <= 10; city++)
//        //{
//        //    _context.Cities.Add(new City() { Id = city, Name = "Город" + city });

//        //    for (int street = 1; street <= 250; street++)
//        //    {
//        //        _context.Streets.Add(new Street { Id = street, Name = "Улица" + street, CityId = city });

//        //        for (int house = 1; house <= 20; house++)
//        //        {
//        //            _context.Houses.Add(new Osi.Domain.House { Id = house, Number = house, StreetId = street });

//        //            for (int flat = 1; flat <= 40; flat++)
//        //            {
//        //                _context.Flat.Add(new Flat { Id = flat, Number = flat, HouseId = house });
//        //                num++;
//        //                if (num == maxSaveString)
//        //                {
//        //                    _context.SaveChangesAsync(cancellationToken);
//        //                }
//        //            }
//        //        }
//        //    }
//        //}


//        //for (int osi = 1; osi <= 500; osi++)
//        //{
//            _context.Osi.Add(new Osi.Domain.Osi { Id = 1, Name = "УК" });
//        //}
//        ((OSIContext)_context).SaveChanges();


//        //DateTime timeEnd = DateTime.Now;

//        //Console.WriteLine(timeEnd - timeStart);
//    }


//    public void Svyaz(CancellationToken cancellationToken)
//    {
//        Random rand = new Random();
//        for (int city = 1; city <= 10; city++)
//        {
//            for (int street = 1; street <= 250; street++)
//            {
//                for (int house = 1; house <= 20; house++)
//                {
//                    for (int flat = 1; flat <= 40; flat++)
//                    {
//                        _context.RangeFlatOsi.Add(new Osi.Domain.RangeFlatOsi
//                        {
//                            OsiId = rand.Next(0, 500),
//                            HouseId = house,

//                            From = 1,
//                            To = 40
//                        });
//                    }
//                }
//            }
//        }
//    }



//}

