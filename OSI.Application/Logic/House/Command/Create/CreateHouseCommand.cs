﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OSI.Application.Exceptions;
using OSI.Application.Responses;
using OSI.Infrastructure.Persistence.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Application.Logic.House.Command.Create
{
    public class CreateHouseCommand : IRequest<Response<int>>
    {
        public int Number { get; set; }
        public int StreetId { get; set; }

    }

    public class CreateHouseCommandHandler : IRequestHandler<CreateHouseCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IOSIContext _context;

        public CreateHouseCommandHandler(IMapper mapper, IOSIContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Response<int>> Handle(CreateHouseCommand request, CancellationToken cancellationToken)
        {
            var isExist = await _context.Doms.AnyAsync(u => u.Number == request.Number);
            var house = _mapper.Map<Osi.Domain.Dom>(request);

            if (!isExist)
            {
                _context.Doms.Add(house);
                var result = await _context.SaveChangesAsync(CancellationToken.None);
                //var resultUserDto = _mapper.Map<UserDto>(user); ;
                return new Response<int>(result);
            }
            else
                throw new ApiException($"{house} not create");
        }
    }
}
