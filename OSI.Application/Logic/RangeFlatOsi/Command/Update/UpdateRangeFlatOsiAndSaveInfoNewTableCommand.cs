﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OSI.Application.Exceptions;
using OSI.Application.Responses;
using OSI.Infrastructure.Persistence.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Application.Logic.RangeFlatOsi.Command.Update
{
    public class UpdateRangeFlatOsiAndSaveInfoNewTableCommand : IRequest<Response<int>>
    {
        public int OsiId { get; set; }
        public int DomId { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public DateTime Date { get; set; }
    }

    public class UpdateRangeFlatOsiAndSaveInfoNewTableCommandHandler : IRequestHandler<UpdateRangeFlatOsiAndSaveInfoNewTableCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IOSIContext _context;

        public UpdateRangeFlatOsiAndSaveInfoNewTableCommandHandler(IMapper mapper, IOSIContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Response<int>> Handle(UpdateRangeFlatOsiAndSaveInfoNewTableCommand request, CancellationToken cancellationToken)
        {
            var isExist = await _context.RangeFlatOsi.AnyAsync(u => u.DomId == request.DomId);

            var rangeFlatOsi = _mapper.Map<Osi.Domain.RangeFlatOsi>(request);

            if (isExist)
            {
                _context.RangeFlatOsi.Update(rangeFlatOsi);

                var saveInfoRFO = _mapper.Map<Osi.Domain.SaveInfoRFO>(rangeFlatOsi);
                _context.SaveInfoRFO.Add(saveInfoRFO);

                var result = await _context.SaveChangesAsync(cancellationToken);
               
                return new Response<int>(result);
            }
            else
                throw new ApiException($"{rangeFlatOsi} not update");
        }
    }
}
