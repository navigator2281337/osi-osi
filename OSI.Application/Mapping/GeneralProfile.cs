﻿using AutoMapper;
using Osi.Domain;
using OSI.Application.Logic;
using OSI.Application.Logic.AddNewAddresInHouse.Command.Create;
using OSI.Application.Logic.House.Command.Create;
using OSI.Application.Logic.RangeFlatOsi.Command;
using OSI.Application.Logic.RangeFlatOsi.Command.Update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSI.Application.Mapping
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Dom, CreateHouseCommand>().ReverseMap();


            CreateMap<Flat, CreateNewAddresInHouseCommand>().ReverseMap();

            
            CreateMap<RangeFlatOsi, CreateRangeFlatOsiCommand>().ReverseMap();


            CreateMap<RangeFlatOsi, UpdateRangeFlatOsiAndSaveInfoNewTableCommand>().ReverseMap();


            CreateMap<RangeFlatOsi, SaveInfoRFO>().ReverseMap();



            //CreateMap<Osi.Domain.Osi, GenerateData>().ReverseMap();
 

            //CreateMap<Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<Osi.Domain.Osi>, Osi.Domain.Osi>().ReverseMap();

            //CreateMap<Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<Street>, Street>().ReverseMap();


        }
    }
}
