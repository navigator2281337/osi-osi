﻿
using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace OSI.Application
{
    public static class ServiceExtencions
    {
        public static void AddApplicationLayer(this IServiceCollection service)
        {
            service.AddAutoMapper(Assembly.GetExecutingAssembly());
            service.AddMediatR(Assembly.GetExecutingAssembly());
        }
    }
}
