﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSI.Infrastructure.Persistence.Interfaces
{
    public interface IGen
    {
        public void AddCities(int numberОfCities = 10);

        public void AddStreets(int number = 250, bool isRandom = false);
        public void AddHouses(int number = 20, bool isRandom = false);
        public void AddFlats(int number = 40, bool isRandom = false);
        public void AddOsi(int number = 500, bool isRandom = false);
        public void SeedDBUsingSeeder();
    }
}
