﻿using Microsoft.EntityFrameworkCore;
using Osi.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Infrastructure.Persistence.Interfaces
{
    public interface IOSIContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Dom> Doms { get; set; }
        public DbSet<Street> Streets { get; set; }
        public DbSet<Osi.Domain.Osi> Osi { get; set; }
        public DbSet<RangeFlatOsi> RangeFlatOsi { get; set; }
        public DbSet<SaveInfoRFO> SaveInfoRFO { get; set; }
        public DbSet<Flat> Flat { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}