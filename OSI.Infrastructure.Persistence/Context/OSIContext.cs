﻿using Microsoft.EntityFrameworkCore;
using Osi.Domain;
using OSI.Infrastructure.Persistence.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSI.Infrastructure.Persistence.Context
{
    public class OSIContext : DbContext, IOSIContext
    {
        public OSIContext()
        {
            
        }

        public OSIContext(DbContextOptions<OSIContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=DBOSI;Trusted_Connection=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<RangeFlatOsi>()
            //  .HasNoKey();

            //modelBuilder.Entity<RangeFlatOsi>().HasKey(ro => new { ro.DomId, ro.OsiId });
            //modelBuilder.Entity<SaveInfoRFO>().HasKey(ro => new { ro.DomId, ro.OsiId });

        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Dom> Doms { get; set; }
        public DbSet<Street> Streets { get; set; }
        public DbSet<Osi.Domain.Osi> Osi { get; set; }  
        public DbSet<RangeFlatOsi> RangeFlatOsi { get; set; }
        public DbSet<SaveInfoRFO> SaveInfoRFO { get; set; }
        public DbSet<Flat> Flat { get; set; }

    }
}
