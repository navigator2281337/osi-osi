﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OSI.Infrastructure.Persistence.Context;
using OSI.Infrastructure.Persistence.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSI.Infrastructure.Persistence
{
    public static class ServiceRegistration
    {
        public static void AddPersistenceService(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddDbContext<OSIContext>(opt =>
                opt.UseSqlServer(configuration.GetConnectionString("OSIConnectionString"),
                b => b.MigrationsAssembly(typeof(OSIContext).Assembly.FullName)));

            //service.AddDbContext<OSIContext>(o =>
            //        o.UseSqlServer(configuration.GetConnectionString("OSIConnectionString"))
            //        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));


            service.AddScoped<IOSIContext>(provider => (IOSIContext)provider.GetService<OSIContext>());

        }
    }
}
