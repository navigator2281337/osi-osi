﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using OSI.Application.Logic;
using OSI.Infrastructure.Persistence.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OSI.Controllers
{
    public class GenDataController : ApiControllerBase
    {

        private readonly IGen _logic;

        public GenDataController(IGen logic)
        {
            _logic = logic;
        }


        //[HttpPost]
        //public async Task<IActionResult> CreateGenerateData(GenerateData command)
        //{
        //    return Ok(await Mediator.Send(command));
        //}


        [HttpPost]
        public void CreateGenerateData()
        {
             _logic.SeedDBUsingSeeder();
        }
    }
}
