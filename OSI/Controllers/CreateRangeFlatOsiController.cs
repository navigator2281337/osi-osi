﻿using Microsoft.AspNetCore.Mvc;
using OSI.Application.Logic.RangeFlatOsi.Command;
using OSI.Application.Logic.RangeFlatOsi.Command.Update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OSI.Controllers
{
    public class CreateRangeFlatOsiController : ApiControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> CreateRangeFlatOsiCommand(CreateRangeFlatOsiCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateeRangeFlatOsiCommand(UpdateRangeFlatOsiAndSaveInfoNewTableCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
