﻿using Microsoft.AspNetCore.Mvc;
using OSI.Application.Logic.AddNewAddresInHouse.Command.Create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OSI.Controllers
{
    public class CreateNewAddresInHouseController : ApiControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> CreateNewAddresInHouseCommand(CreateNewAddresInHouseCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
