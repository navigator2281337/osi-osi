﻿using Microsoft.AspNetCore.Mvc;
using OSI.Application.Logic.House.Command.Create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OSI.Controllers
{
    public class CreateHouseController : ApiControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> CreateHouseCommand(CreateHouseCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
