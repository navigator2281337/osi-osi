﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osi.Domain
{
    public class Flat
    {
        [Key]
        public int Id { get; set; }
        public int Number { get; set; }

        public int DomId { get; set; }
        public Dom Dom { get; set; }
    }
}
