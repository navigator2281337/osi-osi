﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osi.Domain
{
    public class RangeFlatOsi
    {
        [Key]
        public int Id { get; set; }

        public int OsiId { get; set; }
        public Osi Osi { get; set; }

        public int DomId { get; set; }
        public Dom Dom { get; set; }

        public int From { get; set; }
        public int To { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
